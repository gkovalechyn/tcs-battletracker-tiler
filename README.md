# TCS BattleTracker Tiler
## Requirements
* [Pillow](https://pillow.readthedocs.io/en/stable/)

## Usage
```
python Tiler.py <map image> <destination folder> <zoom levels>
```

## Arguments
 * **map image** - The path to the image to be tiled.
 * **destination folder** - Where the tiles should be placed, a folder structure of `{z}/{x}/{y}.png` will be created inside that folder.
 * **zoom levels** A string in the format `<from>-<to>` which represents which zoom levels should be created for that image. By experimentation it seems that it doesn't make sense to create images for a zoom level greater than **6**.

## Example
```
python Tiler.py stratis.png stratis 0-6
```