from PIL import Image
import sys
import argparse
import os
import math

# Disable pillow Image bomb DOS protection since
# we know we're dealing with huge images that will 
# need a lot of memory
Image.MAX_IMAGE_PIXELS = None

# In pixels
TILE_SIZE = 256

def getSideTileCount(zoomLevel: int):
  return 2 ** zoomLevel

argumentParser = argparse.ArgumentParser()

argumentParser.add_argument("image", help="The image to be tiled")
argumentParser.add_argument("folder", help="Where the tiles should be placed")
argumentParser.add_argument("zoomLevel", help="Which zoom levels should be created in the format: <from>-<to>\n Example: 0-5")

args = argumentParser.parse_args()

print (f"Tiling image {args.image} into {args.folder}")

if not os.path.exists(args.folder):
  os.makedirs(args.folder)

print(f"Opening image {args.image}")
sourceImage = Image.open(args.image)

if sourceImage.width % 256 != 0 or sourceImage.height % 256 != 0:
	raise Exception(f"The image width and height must be multiples of 256, image size: ({sourceImage.width}, {sourceImage.height})")

print("Loaded image successfuly")

zoomFrom = int(args.zoomLevel.split("-")[0])
zoomTo = int(args.zoomLevel.split("-")[1])

# + 1 because we want to include the zoom level
for zoomLevel in range(zoomFrom, zoomTo + 1):
  sideTileCount = getSideTileCount(zoomLevel)
  totalTiles = sideTileCount * sideTileCount

  # The image should be a square
  # and we assume that the image is 1px = 1 game unit = 1 meter
  sourceTileSize = sourceImage.size[0] / sideTileCount

  print(f"Generating zoom level {zoomLevel} with a grid of {sideTileCount}x{sideTileCount} ({totalTiles}) tiles")

  # Start at top left and go down
  #  X --> 
  # Y
  # |
  # V
  for tileIndex in range(0, totalTiles):
    tileX = tileIndex % sideTileCount
    tileY = math.floor(tileIndex / float(sideTileCount))

    print(f"tx: {tileX}, ty: {tileY}")
    
    # top left and bottom right
    coordinates = (tileX * sourceTileSize, tileY * sourceTileSize, tileX * sourceTileSize + sourceTileSize, tileY * sourceTileSize + sourceTileSize)
    print(f"Coordinates to resize: {coordinates}")

    resultImage = sourceImage.resize((TILE_SIZE, TILE_SIZE), box=coordinates)
    # Need to invert the Y coordinate because leaflet's coordinate system points down and right
    # Therefore the image with the smallest number is on top
    outputFileName = os.path.join(args.folder, str(zoomLevel), str(tileX), f"{(tileY)}.png")

    if not os.path.exists(os.path.dirname(outputFileName)):
      os.makedirs(os.path.dirname(outputFileName))

    print(f"Writing file \"{outputFileName}\"")
    resultImage.save(outputFileName)

    
